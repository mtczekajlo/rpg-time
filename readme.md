# RPG Time

Simple tool for in-game clock time tracking relative to real time scaled accordingly to session duration.

```bash
❯ poetry run python rpg-time.py --help
usage: RPG in-game time clock [-h] [--real-time-start REAL_TIME_START] [--real-time-end REAL_TIME_END] --game-datetime-start GAME_DATETIME_START --game-time-end
                              GAME_TIME_END [--timeline-file TIMELINE_FILE]

options:
  -h, --help            show this help message and exit
  --real-time-start REAL_TIME_START
                        Session start time e.g. '18:00'
  --real-time-end REAL_TIME_END
                        Session end time e.g. '23:00'
  --game-datetime-start GAME_DATETIME_START
                        Game start datetime e.g. '16:00 24.12.1933'
  --game-time-end GAME_TIME_END
                        Game end time e.g. '20:00'
  --timeline-file TIMELINE_FILE
                        Optional timeline events to show up when their time comes
```
