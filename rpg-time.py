#!/usr/bin/env python3

import argparse
import datetime
import sys
import time


def parse_file(timeline_filename):
  parsed_data = []
  try:
    with open(timeline_filename, "r") as file:
      for raw_line in file:
        line = raw_line.strip()
        if line:
          if line.startswith("*"):
            line.removeprefix("* ")
            parsed_data[-1]["info"].append(line.strip())
          else:
            time_text, event = line.split(" ", 1)
            hour, minute = map(int, time_text.split(":"))
            parsed_data.append(
              {
                "time": datetime.time(hour=hour, minute=minute),
                "event": event,
                "info": [],
              }
            )
    return parsed_data
  except FileNotFoundError as e:
    print(e)
    return None


def main(
  real_datetime_start,
  real_datetime_end,
  game_datetime_start,
  game_datetime_end,
  timeline_filename,
):
  real_to_game_time_multiplier = (game_datetime_end - game_datetime_start) / (real_datetime_end - real_datetime_start)
  print(f"Real time: {real_datetime_start} - {real_datetime_end} (duration: {real_datetime_end-real_datetime_start})")
  print(f"Game time: {game_datetime_start} - {game_datetime_end} (duration: {game_datetime_end-game_datetime_start})")
  print(f"Time factor: {real_to_game_time_multiplier:.02f}")

  events = []
  if timeline_filename:
    print(f"Loading timeline file '{timeline_filename}'")
    events = parse_file(timeline_filename)
    print(f"Loaded {len(events)} events")

  print()

  while True:
    current_time = datetime.datetime.now()

    real_time = (current_time - real_datetime_start).total_seconds()
    delta_time = datetime.timedelta(seconds=real_time * real_to_game_time_multiplier)

    game_time = game_datetime_start + delta_time

    for event in events[:]:
      if event["time"] < game_time.time():
        timepoint = events.pop(0)
        print(f"{timepoint['time'].strftime("%H:%M")} {timepoint['event']}")
        for info in timepoint["info"]:
          print(f"\t{info}")
        print()

    print(
      f"   in-game time: {game_time.strftime("%H:%M:%S %d %B %Y, %A")}\
\t(real time: {current_time.strftime("%H:%M:%S")})",
      end="\r",
    )

    if game_time > game_datetime_end:
      print("\n\n\tGame Over :)")
      break

    time.sleep(1)


if __name__ == "__main__":
  parser = argparse.ArgumentParser(
    prog="RPG in-game time clock",
    description="",
  )

  parser.add_argument(
    "--real-time-start",
    default=None,
    help="Session start time e.g. '18:00'",
  )
  parser.add_argument(
    "--real-time-end",
    default=None,
    help="Session end time e.g. '23:00'",
  )

  parser.add_argument(
    "--game-datetime-start",
    required=True,
    help="Game start datetime e.g. '16:00 24.12.1933'",
  )

  parser.add_argument(
    "--game-time-end",
    required=True,
    help="Game end time e.g. '20:00'",
  )

  parser.add_argument(
    "--timeline-file",
    help="Optional timeline events to show up when their time comes",
  )

  args = parser.parse_args()

  if args.real_time_start:
    real_datetime_start = datetime.datetime.combine(
      datetime.datetime.today(),
      datetime.datetime.strptime(args.real_time_start, "%H:%M").time(),
    )
  else:
    real_datetime_start = datetime.datetime.now()

  if args.real_time_end:
    real_datetime_end = datetime.datetime.combine(
      datetime.datetime.today(),
      datetime.datetime.strptime(args.real_time_end, "%H:%M").time(),
    )
  else:
    real_datetime_end = datetime.datetime.combine(datetime.datetime.today(), datetime.time(hour=23))

  game_datetime_start = datetime.datetime.strptime(args.game_datetime_start, "%H:%M %d.%m.%Y")

  game_datetime_end = datetime.datetime.combine(
    game_datetime_start.date(),
    datetime.datetime.strptime(args.game_time_end, "%H:%M").time(),
  )

  sys.exit(
    main(
      real_datetime_start,
      real_datetime_end,
      game_datetime_start,
      game_datetime_end,
      args.timeline_file,
    )
  )
